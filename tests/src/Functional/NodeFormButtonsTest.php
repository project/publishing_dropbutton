<?php

namespace Drupal\Tests\publishing_dropbutton\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Tests all the different buttons on the node form.
 *
 * @group publishing_dropdown
 */
class NodeFormButtonsTest extends NodeTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'publishing_dropbutton',
  ];

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A normal logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A user with permission to bypass access content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a user that has no access to change the state of the node.
    $this->webUser = $this->drupalCreateUser(['create article content', 'edit own article content']);
    // Create a user that has access to change the state of the node.
    $this->adminUser = $this->drupalCreateUser(['administer nodes', 'bypass node access']);
  }

  /**
   * Tests that the right buttons are displayed for saving nodes.
   */
  public function testNodeFormButtons() {
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    // Log in as administrative user.
    $this->drupalLogin($this->adminUser);

    // Verify the buttons on a node add form.
    $this->drupalGet('node/add/article');
    $this->assertButtons(['Save and publish', 'Save as unpublished']);

    // Save the node and assert it's published after clicking
    // 'Save and publish'.
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save and publish');

    // Get the node.
    $node_1 = $node_storage->load(1);
    $this->assertTrue($node_1->isPublished(), 'Node is published');

    // Verify the buttons on a node edit form.
    $this->drupalGet('node/' . $node_1->id() . '/edit');
    $this->assertButtons(['Save and keep published', 'Save and unpublish']);

    // Save the node and verify it's still published after clicking
    // 'Save and keep published'.
    $this->submitForm($edit, 'Save and keep published');
    $node_storage->resetCache([1]);
    $node_1 = $node_storage->load(1);
    $this->assertTrue($node_1->isPublished(), 'Node is published');
    $this->drupalGet('node/' . $node_1->id() . '/edit');

    // Save the node and verify it's unpublished after clicking
    // 'Save and unpublish'.
    $this->submitForm($edit, 'Save and unpublish');
    $node_storage->resetCache([1]);
    $node_1 = $node_storage->load(1);
    $this->assertFalse($node_1->isPublished(), 'Node is unpublished');

    // Verify the buttons on an unpublished node edit screen.
    $this->drupalGet('node/' . $node_1->id() . '/edit');
    $this->assertButtons(['Save and keep unpublished', 'Save and publish']);

    // Create a node as a normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);

    // Verify the buttons for a normal user.
    $this->drupalGet('node/add/article');
    $this->assertButtons(['Save'], FALSE);

    // Create the node.
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $node_2 = $node_storage->load(2);
    $this->assertTrue($node_2->isPublished(), 'Node is published');

    // Log in as an administrator and unpublish the node that just
    // was created by the normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $node_2->id() . '/edit');
    $this->submitForm([], 'Save and unpublish');
    $node_storage->resetCache([2]);
    $node_2 = $node_storage->load(2);
    $this->assertFalse($node_2->isPublished(), 'Node is unpublished');

    // Log in again as the normal user, save the node and verify
    // it's still unpublished.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $node_2->id() . '/edit');
    $this->submitForm([], 'Save');
    $node_storage->resetCache([2]);
    $node_2 = $node_storage->load(2);
    $this->assertFalse($node_2->isPublished(), 'Node is still unpublished');
    $this->drupalLogout();

    // Set article content type default to unpublished. This will change the
    // the initial order of buttons and/or status of the node when creating
    // a node.
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'article');
    $fields['status']->getConfig('article')
      ->setDefaultValue(FALSE)
      ->save();

    // Verify the buttons on a node add form for an administrator.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/add/article');
    $this->assertButtons(['Save as unpublished', 'Save and publish']);

    // Verify the node is unpublished by default for a normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $node_3 = $node_storage->load(3);
    $this->assertFalse($node_3->isPublished(), 'Node is unpublished');
  }

  /**
   * Assert method to verify the buttons in the dropdown element.
   *
   * @param array $buttons
   *   A collection of buttons to assert for on the page.
   * @param bool $dropbutton
   *   Whether to check if the buttons are in a dropbutton widget or not.
   */
  protected function assertButtons(array $buttons, $dropbutton = TRUE) {
    // Verify that the number of buttons passed as parameters is
    // available in the dropbutton widget.
    if ($dropbutton) {
      $count = count($buttons);

      // Assert there is no save button.
      $this->assertSession()->elementNotExists('named_exact', ['button', 'Save']);

      // Dropbutton elements.
      $this->assertSession()->elementsCount('xpath', '//div[@class="dropbutton-wrapper"]//input[@type="submit"]', $count);
      $this->assertEquals($buttons, array_map(function (NodeElement $button) {
        return $button->getValue();
      }, $this->getSession()
        ->getPage()
        ->findAll('css', '.dropbutton-wrapper input[type=submit]')));
      return;
    }
    $this->assertSession()->elementExists('named_exact', ['button', 'Save']);
    $this->assertSession()->responseNotContains('dropbutton-wrapper');
  }

}
